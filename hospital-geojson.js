var hospital = {
  "type": "FeatureCollection",
  "features": [
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.39072342997645,
              -38.184658252252255
            ],
            [
              144.39581057002357,
              -38.184658252252255
            ],
            [
              144.39581057002357,
              -38.180153747747745
            ],
            [
              144.39072342997645,
              -38.180153747747745
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 927,
        "hospital_name": "The Geelong Clinic"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5828"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.9698780256453,
              -37.836498252252255
            ],
            [
              144.9744239743547,
              -37.836498252252255
            ],
            [
              144.9744239743547,
              -37.831993747747745
            ],
            [
              144.9698780256453,
              -37.831993747747745
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 8,
        "hospital_name": "Albert Road Clinic"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5829"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.71357086471158,
              -37.197317252252255
            ],
            [
              145.7187151352884,
              -37.197317252252255
            ],
            [
              145.7187151352884,
              -37.192812747747745
            ],
            [
              145.71357086471158,
              -37.192812747747745
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 12,
        "hospital_name": "Alexandra District Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_582a"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.31186814837696,
              -37.900915252252254
            ],
            [
              145.31646385162304,
              -37.900915252252254
            ],
            [
              145.31646385162304,
              -37.896410747747744
            ],
            [
              145.31186814837696,
              -37.896410747747744
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 22,
        "hospital_name": "Angliss Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_582b"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.05798005263642,
              -37.758607252252254
            ],
            [
              145.06249194736358,
              -37.758607252252254
            ],
            [
              145.06249194736358,
              -37.754102747747744
            ],
            [
              145.05798005263642,
              -37.754102747747744
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": ">500",
        "bed_cat": 5,
        "id": 34,
        "hospital_name": "Austin Hospital [Heidelberg]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_582c"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.99593269967076,
              -37.85837025225226
            ],
            [
              145.00049330032923,
              -37.85837025225226
            ],
            [
              145.00049330032923,
              -37.85386574774775
            ],
            [
              144.99593269967076,
              -37.85386574774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 35,
        "hospital_name": "Avenue Plastic Surgery"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_582d"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.0465311421685,
              -38.22773625225226
            ],
            [
              145.0517408578315,
              -38.22773625225226
            ],
            [
              145.0517408578315,
              -38.22323174774775
            ],
            [
              145.0465311421685,
              -38.22323174774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 69,
        "hospital_name": "Beleura Private Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_582e"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.15374538652802,
              -37.835325252252254
            ],
            [
              145.15829061347196,
              -37.835325252252254
            ],
            [
              145.15829061347196,
              -37.830820747747744
            ],
            [
              145.15374538652802,
              -37.830820747747744
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 70,
        "hospital_name": "Bellbird Private Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_582f"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.34410254514606,
              -38.035919252252256
            ],
            [
              145.34887145485393,
              -38.035919252252256
            ],
            [
              145.34887145485393,
              -38.031414747747746
            ],
            [
              145.34410254514606,
              -38.031414747747746
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 78,
        "hospital_name": "Berwick Eye and Surgicentre"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5830"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.1168268845744,
              -37.815934252252255
            ],
            [
              145.12136111542557,
              -37.815934252252255
            ],
            [
              145.12136111542557,
              -37.811429747747745
            ],
            [
              145.1168268845744,
              -37.811429747747745
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "200-500",
        "bed_cat": 4,
        "id": 105,
        "hospital_name": "Box Hill Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5831"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.91085350136376,
              -37.68657325225225
            ],
            [
              144.91535849863624,
              -37.68657325225225
            ],
            [
              144.91535849863624,
              -37.68206874774774
            ],
            [
              144.91085350136376,
              -37.68206874774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 115,
        "hospital_name": "Broadmeadows Health Service"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5832"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.0533167431883,
              -37.703376252252255
            ],
            [
              145.05782125681168,
              -37.703376252252255
            ],
            [
              145.05782125681168,
              -37.698871747747745
            ],
            [
              145.0533167431883,
              -37.698871747747745
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 122,
        "hospital_name": "Bundoora Extended Care Centre"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5833"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.9892679024681,
              -37.91390225225226
            ],
            [
              144.99387609753188,
              -37.91390225225226
            ],
            [
              144.99387609753188,
              -37.90939774774775
            ],
            [
              144.9892679024681,
              -37.90939774774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 130,
        "hospital_name": "Cabrini Brighton"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5834"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.0070360813428,
              -37.887682252252255
            ],
            [
              145.01161991865723,
              -37.887682252252255
            ],
            [
              145.01161991865723,
              -37.883177747747744
            ],
            [
              145.0070360813428,
              -37.883177747747744
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 131,
        "hospital_name": "Cabrini Health Elsternwick Rehabilitation - Glenhuntly Rd"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5835"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.0080983625092,
              -37.88703025225225
            ],
            [
              145.0126816374908,
              -37.88703025225225
            ],
            [
              145.0126816374908,
              -37.88252574774774
            ],
            [
              145.0080983625092,
              -37.88252574774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 132,
        "hospital_name": "Cabrini Health Elsternwick Rehabilitation - Hopetoun"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5836"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.03121766933552,
              -37.863893252252254
            ],
            [
              145.0357823306645,
              -37.863893252252254
            ],
            [
              145.0357823306645,
              -37.859388747747744
            ],
            [
              145.03121766933552,
              -37.859388747747744
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": ">500",
        "bed_cat": 5,
        "id": 133,
        "hospital_name": "Cabrini Malvern"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5837"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.00673744678167,
              -37.856286252252254
            ],
            [
              145.01129655321833,
              -37.856286252252254
            ],
            [
              145.01129655321833,
              -37.851781747747744
            ],
            [
              145.00673744678167,
              -37.851781747747744
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 134,
        "hospital_name": "Cabrini Prahran"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5838"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.01111793527812,
              -37.89921425225226
            ],
            [
              145.0157120647219,
              -37.89921425225226
            ],
            [
              145.0157120647219,
              -37.89470974774775
            ],
            [
              145.01111793527812,
              -37.89470974774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 141,
        "hospital_name": "Calvary Health Care Bethlehem"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5839"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.9683589756036,
              -37.81167125225225
            ],
            [
              144.97289102439643,
              -37.81167125225225
            ],
            [
              144.97289102439643,
              -37.80716674774774
            ],
            [
              144.9683589756036,
              -37.80716674774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": null,
        "bed_cat": null,
        "id": 165,
        "hospital_name": "Careplans Assessment Victoria"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_583a"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.01329213269804,
              -37.80695825225226
            ],
            [
              145.01782186730196,
              -37.80695825225226
            ],
            [
              145.01782186730196,
              -37.80245374774775
            ],
            [
              145.01329213269804,
              -37.80245374774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 166,
        "hospital_name": "Caritas Christi Hospice [Kew]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_583b"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.3447866794817,
              -38.04757725225225
            ],
            [
              145.3495753205183,
              -38.04757725225225
            ],
            [
              145.3495753205183,
              -38.04307274774774
            ],
            [
              145.3447866794817,
              -38.04307274774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "200-500",
        "bed_cat": 4,
        "id": 168,
        "hospital_name": "Casey Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_583c"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.01460124868618,
              -37.88495925225226
            ],
            [
              145.01918275131385,
              -37.88495925225226
            ],
            [
              145.01918275131385,
              -37.88045474774775
            ],
            [
              145.01460124868618,
              -37.88045474774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "200-500",
        "bed_cat": 4,
        "id": 174,
        "hospital_name": "Caulfield Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_583d"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.05446857853866,
              -37.96271225225225
            ],
            [
              145.05913142146136,
              -37.96271225225225
            ],
            [
              145.05913142146136,
              -37.95820774774774
            ],
            [
              145.05446857853866,
              -37.95820774774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 187,
        "hospital_name": "Chesterville Day Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_583e"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.07022239161765,
              -37.99308725225225
            ],
            [
              145.07492560838236,
              -37.99308725225225
            ],
            [
              145.07492560838236,
              -37.98858274774774
            ],
            [
              145.07022239161765,
              -37.98858274774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 208,
        "hospital_name": "Como Private Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_583f"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.21480776311907,
              -37.979453252252256
            ],
            [
              145.21949223688094,
              -37.979453252252256
            ],
            [
              145.21949223688094,
              -37.974948747747746
            ],
            [
              145.21480776311907,
              -37.974948747747746
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 224,
        "hospital_name": "Corymbia House"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5840"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.04603915695216,
              -37.81094625225226
            ],
            [
              145.05057084304784,
              -37.81094625225226
            ],
            [
              145.05057084304784,
              -37.80644174774775
            ],
            [
              145.04603915695216,
              -37.80644174774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 225,
        "hospital_name": "Cotham Private Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5841"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.91701572495919,
              -37.598267252252256
            ],
            [
              144.9215442750408,
              -37.598267252252256
            ],
            [
              144.9215442750408,
              -37.593762747747746
            ],
            [
              144.91701572495919,
              -37.593762747747746
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 229,
        "hospital_name": "Craigieburn Health Service"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5842"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.27837170123402,
              -38.115564252252256
            ],
            [
              145.28329229876599,
              -38.115564252252256
            ],
            [
              145.28329229876599,
              -38.111059747747746
            ],
            [
              145.27837170123402,
              -38.111059747747746
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 230,
        "hospital_name": "Cranbourne Integrated Care Centre"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5843"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.7400491181142,
              -37.78355925225225
            ],
            [
              144.7445688818858,
              -37.78355925225225
            ],
            [
              144.7445688818858,
              -37.77905474774774
            ],
            [
              144.7400491181142,
              -37.77905474774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 246,
        "hospital_name": "Dame Phyllis Frost Centre - Marmak unit"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5844"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.21618725852795,
              -37.97871125225225
            ],
            [
              145.22087074147203,
              -37.97871125225225
            ],
            [
              145.22087074147203,
              -37.97420674774774
            ],
            [
              145.21618725852795,
              -37.97420674774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "200-500",
        "bed_cat": 4,
        "id": 247,
        "hospital_name": "Dandenong Campus"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5845"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.96226539682664,
              -37.80155125225225
            ],
            [
              144.96679260317336,
              -37.80155125225225
            ],
            [
              144.96679260317336,
              -37.79704674774774
            ],
            [
              144.96226539682664,
              -37.79704674774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 259,
        "hospital_name": "Dental Health Services Victoria"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5846"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.43101125834892,
              -37.68051925225225
            ],
            [
              144.4355167416511,
              -37.68051925225225
            ],
            [
              144.4355167416511,
              -37.67601474774774
            ],
            [
              144.43101125834892,
              -37.67601474774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 265,
        "hospital_name": "Djerriwarrh Health Service [Bacchus Marsh]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5847"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.16939344685193,
              -37.792088252252256
            ],
            [
              145.17391655314805,
              -37.792088252252256
            ],
            [
              145.17391655314805,
              -37.787583747747746
            ],
            [
              145.16939344685193,
              -37.787583747747746
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 268,
        "hospital_name": "Donvale Rehabilitation Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5848"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.94518308315676,
              -37.725654252252255
            ],
            [
              144.94968891684326,
              -37.725654252252255
            ],
            [
              144.94968891684326,
              -37.721149747747745
            ],
            [
              144.94518308315676,
              -37.721149747747745
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 271,
        "hospital_name": "Dorset Rehabilitation Centre"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5849"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.9852793634562,
              -37.81791425225226
            ],
            [
              144.98981463654383,
              -37.81791425225226
            ],
            [
              144.98981463654383,
              -37.81340974774775
            ],
            [
              144.9852793634562,
              -37.81340974774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 295,
        "hospital_name": "Epworth Cliveden"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_584a"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.1171006235772,
              -37.816930252252256
            ],
            [
              145.12163537642283,
              -37.816930252252256
            ],
            [
              145.12163537642283,
              -37.812425747747746
            ],
            [
              145.1171006235772,
              -37.812425747747746
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "200-500",
        "bed_cat": 4,
        "id": 296,
        "hospital_name": "Epworth Eastern"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_584b"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.98184065371981,
              -37.81294625225225
            ],
            [
              144.98637334628017,
              -37.81294625225225
            ],
            [
              144.98637334628017,
              -37.80844174774774
            ],
            [
              144.98184065371981,
              -37.80844174774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 297,
        "hospital_name": "Epworth Freemasons [Clarendon Street]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_584c"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.9798540094644,
              -37.81153625225225
            ],
            [
              144.98438599053563,
              -37.81153625225225
            ],
            [
              144.98438599053563,
              -37.80703174774774
            ],
            [
              144.9798540094644,
              -37.80703174774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 298,
        "hospital_name": "Epworth Freemasons [Victoria Parade]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_584d"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.02113584929498,
              -37.82348125225226
            ],
            [
              145.02567415070502,
              -37.82348125225226
            ],
            [
              145.02567415070502,
              -37.81897674774775
            ],
            [
              145.02113584929498,
              -37.81897674774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 299,
        "hospital_name": "Epworth Hawthorn"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_584e"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.0005098834008,
              -37.91594125225225
            ],
            [
              145.0051201165992,
              -37.91594125225225
            ],
            [
              145.0051201165992,
              -37.91143674774774
            ],
            [
              145.0005098834008,
              -37.91143674774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 300,
        "hospital_name": "Epworth Rehabilitation Brighton"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_584f"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.0524121547563,
              -37.84847525225226
            ],
            [
              145.0569658452437,
              -37.84847525225226
            ],
            [
              145.0569658452437,
              -37.84397074774775
            ],
            [
              145.0524121547563,
              -37.84397074774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 301,
        "hospital_name": "Epworth Rehabilitation Camberwell"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5850"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.99123489338123,
              -37.819671252252256
            ],
            [
              144.99577110661878,
              -37.819671252252256
            ],
            [
              144.99577110661878,
              -37.815166747747746
            ],
            [
              144.99123489338123,
              -37.815166747747746
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 302,
        "hospital_name": "Epworth Rehabilitation Richmond"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5851"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.99076088960672,
              -37.819685252252256
            ],
            [
              144.9952971103933,
              -37.819685252252256
            ],
            [
              144.9952971103933,
              -37.815180747747746
            ],
            [
              144.99076088960672,
              -37.815180747747746
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "200-500",
        "bed_cat": 4,
        "id": 303,
        "hospital_name": "Epworth Richmond"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5852"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.952871519591,
              -37.801010252252254
            ],
            [
              144.957398480409,
              -37.801010252252254
            ],
            [
              144.957398480409,
              -37.796505747747744
            ],
            [
              144.952871519591,
              -37.796505747747744
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 322,
        "hospital_name": "Frances Perry House"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5853"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.1259247060613,
              -38.15304325225225
            ],
            [
              145.13093129393872,
              -38.15304325225225
            ],
            [
              145.13093129393872,
              -38.14853874774774
            ],
            [
              145.1259247060613,
              -38.14853874774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "200-500",
        "bed_cat": 4,
        "id": 323,
        "hospital_name": "Frankston Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5854"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.36245980394963,
              -38.153785252252256
            ],
            [
              144.36746819605034,
              -38.153785252252256
            ],
            [
              144.36746819605034,
              -38.149280747747746
            ],
            [
              144.36245980394963,
              -38.149280747747746
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "200-500",
        "bed_cat": 4,
        "id": 330,
        "hospital_name": "Geelong Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5855"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.36158151170739,
              -38.153203252252254
            ],
            [
              144.3665884882926,
              -38.153203252252254
            ],
            [
              144.3665884882926,
              -38.148698747747744
            ],
            [
              144.36158151170739,
              -38.148698747747744
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 331,
        "hospital_name": "Geelong Private Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5856"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.82522325482392,
              -38.43258025225226
            ],
            [
              145.83127474517607,
              -38.43258025225226
            ],
            [
              145.83127474517607,
              -38.42807574774775
            ],
            [
              145.82522325482392,
              -38.42807574774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 338,
        "hospital_name": "Gippsland Southern Health Service - Korumburra"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5857"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.01696634818668,
              -37.902629252252254
            ],
            [
              145.0215636518133,
              -37.902629252252254
            ],
            [
              145.0215636518133,
              -37.898124747747744
            ],
            [
              145.01696634818668,
              -37.898124747747744
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 341,
        "hospital_name": "Glen Eira Day Surgery"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5858"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.02927533855834,
              -37.82171125225226
            ],
            [
              145.03381266144166,
              -37.82171125225226
            ],
            [
              145.03381266144166,
              -37.81720674774775
            ],
            [
              145.02927533855834,
              -37.81720674774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 344,
        "hospital_name": "Glenferrie Private Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5859"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.14723723189928,
              -38.17569925225226
            ],
            [
              145.1523007681007,
              -38.17569925225226
            ],
            [
              145.1523007681007,
              -38.17119474774775
            ],
            [
              145.14723723189928,
              -38.17119474774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 351,
        "hospital_name": "Golf Links Road Rehabilitation Centre"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_585a"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.52677667187524,
              -37.64913125225225
            ],
            [
              145.53128732812476,
              -37.64913125225225
            ],
            [
              145.53128732812476,
              -37.64462674774774
            ],
            [
              145.52677667187524,
              -37.64462674774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 387,
        "hospital_name": "Healesville and District Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_585b"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.07884824993783,
              -37.95007025225225
            ],
            [
              145.08349575006218,
              -37.95007025225225
            ],
            [
              145.08349575006218,
              -37.94556574774774
            ],
            [
              145.07884824993783,
              -37.94556574774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": null,
        "bed_cat": null,
        "id": 388,
        "hospital_name": "Healthscope Independence Services"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_585c"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.04523410185527,
              -37.75822525225225
            ],
            [
              145.04974589814475,
              -37.75822525225225
            ],
            [
              145.04974589814475,
              -37.75372074774774
            ],
            [
              145.04523410185527,
              -37.75372074774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 392,
        "hospital_name": "Heidelberg Repatriation Hospital [Heidelberg West]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_585d"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.82620867414497,
              -37.87170025225225
            ],
            [
              144.83077932585505,
              -37.87170025225225
            ],
            [
              144.83077932585505,
              -37.86719574774774
            ],
            [
              144.82620867414497,
              -37.86719574774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": null,
        "bed_cat": null,
        "id": 402,
        "hospital_name": "Hobsons Bay Endoscopy Centre Altona"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_585e"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.75642471055556,
              -37.695617252252255
            ],
            [
              144.76092928944445,
              -37.695617252252255
            ],
            [
              144.76092928944445,
              -37.691112747747745
            ],
            [
              144.75642471055556,
              -37.691112747747745
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": null,
        "bed_cat": null,
        "id": 403,
        "hospital_name": "Hobsons Bay Endoscopy Centre Sydenham"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_585f"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.6841997930561,
              -37.89290125225226
            ],
            [
              144.6887882069439,
              -37.89290125225226
            ],
            [
              144.6887882069439,
              -37.88839674774775
            ],
            [
              144.6841997930561,
              -37.88839674774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": null,
        "bed_cat": null,
        "id": 404,
        "hospital_name": "Hobsons Bay Endoscopy Centre Werribee"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5860"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.95614230490676,
              -37.75662125225225
            ],
            [
              144.9606536950932,
              -37.75662125225225
            ],
            [
              144.9606536950932,
              -37.75211674774774
            ],
            [
              144.95614230490676,
              -37.75211674774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 433,
        "hospital_name": "John Fawkner Private Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5861"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.97632807000005,
              -37.81901425225225
            ],
            [
              144.98086392999997,
              -37.81901425225225
            ],
            [
              144.98086392999997,
              -37.81450974774774
            ],
            [
              144.97632807000005,
              -37.81450974774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 436,
        "hospital_name": "Jolimont Endoscopy"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5862"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.07664093266231,
              -37.95727025225226
            ],
            [
              145.0812970673377,
              -37.95727025225226
            ],
            [
              145.0812970673377,
              -37.95276574774775
            ],
            [
              145.07664093266231,
              -37.95276574774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 468,
        "hospital_name": "Kingston Centre [Cheltenham]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5863"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.22575698629083,
              -37.85189725225226
            ],
            [
              145.23031301370918,
              -37.85189725225226
            ],
            [
              145.23031301370918,
              -37.84739274774775
            ],
            [
              145.22575698629083,
              -37.84739274774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "200-500",
        "bed_cat": 4,
        "id": 471,
        "hospital_name": "Knox Private Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5864"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.48186440659015,
              -38.20290525225226
            ],
            [
              145.48700159340984,
              -38.20290525225226
            ],
            [
              145.48700159340984,
              -38.19840074774775
            ],
            [
              145.48186440659015,
              -38.19840074774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 474,
        "hospital_name": "Kooweerup Regional Health Service"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5865"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.46724711522077,
              -37.25668325225225
            ],
            [
              144.47223688477922,
              -37.25668325225225
            ],
            [
              144.47223688477922,
              -37.25217874774774
            ],
            [
              144.46724711522077,
              -37.25217874774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 481,
        "hospital_name": "Kyneton District Health Service"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5866"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.0009382366092,
              -37.94495125225225
            ],
            [
              145.00557976339084,
              -37.94495125225225
            ],
            [
              145.00557976339084,
              -37.94044674774774
            ],
            [
              145.0009382366092,
              -37.94044674774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 499,
        "hospital_name": "Linacre Private Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5867"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.11340350669855,
              -37.775723252252256
            ],
            [
              145.11792049330143,
              -37.775723252252256
            ],
            [
              145.11792049330143,
              -37.771218747747746
            ],
            [
              145.11340350669855,
              -37.771218747747746
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": null,
        "bed_cat": null,
        "id": 531,
        "hospital_name": "Manningham Day Procedure Centre"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5868"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.25226960437527,
              -37.80913625225225
            ],
            [
              145.25680039562474,
              -37.80913625225225
            ],
            [
              145.25680039562474,
              -37.80463174774774
            ],
            [
              145.25226960437527,
              -37.80463174774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "200-500",
        "bed_cat": 4,
        "id": 538,
        "hospital_name": "Maroondah Hospital [East Ringwood]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5869"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.00081053602383,
              -37.872051252252255
            ],
            [
              145.00538146397616,
              -37.872051252252255
            ],
            [
              145.00538146397616,
              -37.867546747747745
            ],
            [
              145.00081053602383,
              -37.867546747747745
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 544,
        "hospital_name": "Masada Private Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_586a"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.01648882491028,
              -37.87131625225226
            ],
            [
              145.02105917508973,
              -37.87131625225226
            ],
            [
              145.02105917508973,
              -37.86681174774775
            ],
            [
              145.01648882491028,
              -37.86681174774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 565,
        "hospital_name": "Melbourne MediBrain & MediSleep Centre"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_586b"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.96912367354406,
              -37.81674025225225
            ],
            [
              144.97365832645596,
              -37.81674025225225
            ],
            [
              144.97365832645596,
              -37.81223574774774
            ],
            [
              144.96912367354406,
              -37.81223574774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 566,
        "hospital_name": "Melbourne Oral & Facial Surgery"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_586c"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.9547616292985,
              -37.80052425225225
            ],
            [
              144.95928837070147,
              -37.80052425225225
            ],
            [
              144.95928837070147,
              -37.79601974774774
            ],
            [
              144.9547616292985,
              -37.79601974774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 567,
        "hospital_name": "Melbourne Private Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_586d"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.55603156716404,
              -37.68870125225226
            ],
            [
              144.56053643283593,
              -37.68870125225226
            ],
            [
              144.56053643283593,
              -37.68419674774775
            ],
            [
              144.55603156716404,
              -37.68419674774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 568,
        "hospital_name": "Melton Health"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_586e"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.05946108983014,
              -37.818940252252254
            ],
            [
              145.0639969101699,
              -37.818940252252254
            ],
            [
              145.0639969101699,
              -37.814435747747744
            ],
            [
              145.05946108983014,
              -37.814435747747744
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 573,
        "hospital_name": "Mercy Health - O''Connell Family Centre"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_586f"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.05875208874545,
              -37.75832725225226
            ],
            [
              145.06326391125452,
              -37.75832725225226
            ],
            [
              145.06326391125452,
              -37.75382274774775
            ],
            [
              145.05875208874545,
              -37.75382274774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 574,
        "hospital_name": "Mercy Hospital for Women"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5870"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.19254171915864,
              -37.81268825225226
            ],
            [
              145.19707428084135,
              -37.81268825225226
            ],
            [
              145.19707428084135,
              -37.80818374774775
            ],
            [
              145.19254171915864,
              -37.80818374774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 589,
        "hospital_name": "Mitcham Private Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5871"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.12081905130577,
              -37.92342825225226
            ],
            [
              145.12543694869424,
              -37.92342825225226
            ],
            [
              145.12543694869424,
              -37.91892374774775
            ],
            [
              145.12081905130577,
              -37.91892374774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": ">500",
        "bed_cat": 5,
        "id": 595,
        "hospital_name": "Monash Medical Centre [Clayton]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5872"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.0608883007322,
              -37.92294925225225
            ],
            [
              145.06550569926782,
              -37.92294925225225
            ],
            [
              145.06550569926782,
              -37.91844474774774
            ],
            [
              145.0608883007322,
              -37.91844474774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 596,
        "hospital_name": "Monash Medical Centre [Moorabbin]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5873"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.08483291109218,
              -38.17052125225226
            ],
            [
              145.0898830889078,
              -38.17052125225226
            ],
            [
              145.0898830889078,
              -38.16601674774775
            ],
            [
              145.08483291109218,
              -38.16601674774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 614,
        "hospital_name": "Mount Eliza Aged Care & Rehabilitation Service"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5874"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.02908708198936,
              -37.765676252252256
            ],
            [
              145.03360091801062,
              -37.765676252252256
            ],
            [
              145.03360091801062,
              -37.761171747747746
            ],
            [
              145.02908708198936,
              -37.761171747747746
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 668,
        "hospital_name": "North Eastern Rehabilitation Centre"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5875"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.05937867739598,
              -37.69346025225226
            ],
            [
              145.06388332260403,
              -37.69346025225226
            ],
            [
              145.06388332260403,
              -37.68895574774775
            ],
            [
              145.05937867739598,
              -37.68895574774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 679,
        "hospital_name": "Northpark Private Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5876"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.88435501182786,
              -37.79417825225225
            ],
            [
              144.88887898817214,
              -37.79417825225225
            ],
            [
              144.88887898817214,
              -37.78967374774774
            ],
            [
              144.88435501182786,
              -37.78967374774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 699,
        "hospital_name": "Orygen Inpatient Unit"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5877"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.1670404046398,
              -38.15980925225225
            ],
            [
              145.1720635953602,
              -38.15980925225225
            ],
            [
              145.1720635953602,
              -38.15530474774774
            ],
            [
              145.1670404046398,
              -38.15530474774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 714,
        "hospital_name": "Peninsula Private Hospital Victoria"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5878"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.97510542551413,
              -37.81384125225225
            ],
            [
              144.97963857448588,
              -37.81384125225225
            ],
            [
              144.97963857448588,
              -37.80933674774774
            ],
            [
              144.97510542551413,
              -37.80933674774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 719,
        "hospital_name": "Peter MacCallum Cancer Institute"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5879"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.17594616945658,
              -37.97430525225226
            ],
            [
              145.1806238305434,
              -37.97430525225226
            ],
            [
              145.1806238305434,
              -37.96980074774775
            ],
            [
              145.17594616945658,
              -37.96980074774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 746,
        "hospital_name": "Queen Elizabeth Centre [Noble Park]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_587a"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.24136244240628,
              -37.813775252252256
            ],
            [
              145.2458955575937,
              -37.813775252252256
            ],
            [
              145.2458955575937,
              -37.809270747747746
            ],
            [
              145.24136244240628,
              -37.809270747747746
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 761,
        "hospital_name": "Ringwood Private Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_587b"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.88204122806664,
              -38.36442025225225
            ],
            [
              144.88775677193334,
              -38.36442025225225
            ],
            [
              144.88775677193334,
              -38.35991574774774
            ],
            [
              144.88204122806664,
              -38.35991574774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 774,
        "hospital_name": "Rosebud Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_587c"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.89435184613075,
              -38.36904125225225
            ],
            [
              144.90008815386926,
              -38.36904125225225
            ],
            [
              144.90008815386926,
              -38.36453674774774
            ],
            [
              144.89435184613075,
              -38.36453674774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": null,
        "bed_cat": null,
        "id": 775,
        "hospital_name": "Rosebud Rehabilitation Unit"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_587d"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.88069716492998,
              -38.364000252252254
            ],
            [
              144.88641083507002,
              -38.364000252252254
            ],
            [
              144.88641083507002,
              -38.359495747747744
            ],
            [
              144.88069716492998,
              -38.359495747747744
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": null,
        "bed_cat": null,
        "id": 776,
        "hospital_name": "Rosebud SurgiCentre"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_587e"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.948596354374,
              -37.79724925225226
            ],
            [
              144.95312164562603,
              -37.79724925225226
            ],
            [
              144.95312164562603,
              -37.79274474774775
            ],
            [
              144.948596354374,
              -37.79274474774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "200-500",
        "bed_cat": 4,
        "id": 781,
        "hospital_name": "Royal Children''s Hospital [Parkville]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_587f"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.93297595621942,
              -37.78442425225226
            ],
            [
              144.93749604378058,
              -37.78442425225226
            ],
            [
              144.93749604378058,
              -37.77991974774775
            ],
            [
              144.93297595621942,
              -37.77991974774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": null,
        "bed_cat": null,
        "id": 782,
        "hospital_name": "Royal Children''s Hospital [Travancore Psych]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5880"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.9546004059266,
              -37.801511252252254
            ],
            [
              144.95912759407338,
              -37.801511252252254
            ],
            [
              144.95912759407338,
              -37.797006747747744
            ],
            [
              144.9546004059266,
              -37.797006747747744
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": ">500",
        "bed_cat": 5,
        "id": 786,
        "hospital_name": "Royal Melbourne Hospital [Parkville]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5881"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.94578957530288,
              -37.781065252252255
            ],
            [
              144.9503084246971,
              -37.781065252252255
            ],
            [
              144.9503084246971,
              -37.776560747747745
            ],
            [
              144.94578957530288,
              -37.776560747747745
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 787,
        "hospital_name": "Royal Melbourne Hospital- Royal Park Campus"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5882"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.0214535113417,
              -37.79177425225225
            ],
            [
              145.02597648865833,
              -37.79177425225225
            ],
            [
              145.02597648865833,
              -37.78726974774774
            ],
            [
              145.0214535113417,
              -37.78726974774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 794,
        "hospital_name": "Royal Talbot Rehabilitation Centre [Kew]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5883"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.95256151642263,
              -37.801024252252255
            ],
            [
              144.95708848357737,
              -37.801024252252255
            ],
            [
              144.95708848357737,
              -37.796519747747745
            ],
            [
              144.95256151642263,
              -37.796519747747745
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 795,
        "hospital_name": "Royal Women''s Hospital [Parkville]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5884"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.01582521079055,
              -37.96330125225226
            ],
            [
              145.02048878920945,
              -37.96330125225226
            ],
            [
              145.02048878920945,
              -37.95879674774775
            ],
            [
              145.01582521079055,
              -37.95879674774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 803,
        "hospital_name": "Sandringham Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5885"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.18958692773123,
              -37.968495252252254
            ],
            [
              145.19425707226878,
              -37.968495252252254
            ],
            [
              145.19425707226878,
              -37.963990747747744
            ],
            [
              145.18958692773123,
              -37.963990747747744
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 821,
        "hospital_name": "South Eastern Private"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5886"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.05031905930056,
              -37.81133725225226
            ],
            [
              145.05485094069942,
              -37.81133725225226
            ],
            [
              145.05485094069942,
              -37.80683274774775
            ],
            [
              145.05031905930056,
              -37.80683274774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 844,
        "hospital_name": "St George''s Health Service- Aged Care"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5887"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.34258885448438,
              -38.03675125225225
            ],
            [
              145.34735914551564,
              -38.03675125225225
            ],
            [
              145.34735914551564,
              -38.03224674774774
            ],
            [
              145.34258885448438,
              -38.03224674774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 849,
        "hospital_name": "St John of God Berwick Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5888"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.1534199716248,
              -38.15528725225226
            ],
            [
              145.1584320283752,
              -38.15528725225226
            ],
            [
              145.1584320283752,
              -38.15078274774775
            ],
            [
              145.1534199716248,
              -38.15078274774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 852,
        "hospital_name": "St John of God Frankston Rehabilitation Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5889"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.3551747625644,
              -38.153819252252255
            ],
            [
              144.3601832374356,
              -38.153819252252255
            ],
            [
              144.3601832374356,
              -38.149314747747745
            ],
            [
              144.3551747625644,
              -38.149314747747745
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 853,
        "hospital_name": "St John of God Geelong Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_588a"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.21080536108596,
              -37.97247825225225
            ],
            [
              145.21548063891404,
              -37.97247825225225
            ],
            [
              145.21548063891404,
              -37.96797374774774
            ],
            [
              145.21080536108596,
              -37.96797374774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 857,
        "hospital_name": "St John of God Pinelodge Clinic"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_588b"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.9729656873297,
              -37.808797252252255
            ],
            [
              144.97749631267033,
              -37.808797252252255
            ],
            [
              144.97749631267033,
              -37.804292747747745
            ],
            [
              144.9729656873297,
              -37.804292747747745
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": ">500",
        "bed_cat": 5,
        "id": 869,
        "hospital_name": "St Vincent''s Hospital [Fitzroy]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_588c"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.9818973886234,
              -37.81398525225225
            ],
            [
              144.9864306113766,
              -37.81398525225225
            ],
            [
              144.9864306113766,
              -37.80948074774774
            ],
            [
              144.9818973886234,
              -37.80948074774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 872,
        "hospital_name": "St Vincent''s Private Hospital East Melbourne"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_588d"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.9737633311376,
              -37.81024525225226
            ],
            [
              144.97829466886242,
              -37.81024525225226
            ],
            [
              144.97829466886242,
              -37.80574074774775
            ],
            [
              144.9737633311376,
              -37.80574074774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "200-500",
        "bed_cat": 4,
        "id": 873,
        "hospital_name": "St Vincent''s Private Hospital Fitzroy"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_588e"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.0214286819543,
              -37.808819252252256
            ],
            [
              145.02595931804572,
              -37.808819252252256
            ],
            [
              145.02595931804572,
              -37.804314747747746
            ],
            [
              145.0214286819543,
              -37.804314747747746
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 874,
        "hospital_name": "St Vincent''s Private Hospital Kew"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_588f"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.73040485021784,
              -37.57925025225226
            ],
            [
              144.73494314978217,
              -37.57925025225226
            ],
            [
              144.73494314978217,
              -37.57474574774775
            ],
            [
              144.73040485021784,
              -37.57474574774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 886,
        "hospital_name": "Sunbury Day Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5890"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.814220678496,
              -37.76143125225226
            ],
            [
              144.81873332150397,
              -37.76143125225226
            ],
            [
              144.81873332150397,
              -37.75692674774775
            ],
            [
              144.814220678496,
              -37.75692674774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "200-500",
        "bed_cat": 4,
        "id": 890,
        "hospital_name": "Sunshine Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5891"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.96130354152757,
              -37.76243225225225
            ],
            [
              144.96581645847243,
              -37.76243225225225
            ],
            [
              144.96581645847243,
              -37.75792774774774
            ],
            [
              144.96130354152757,
              -37.75792774774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": null,
        "bed_cat": null,
        "id": 902,
        "hospital_name": "Sydney Road Clinic"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5892"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.9796602006189,
              -37.84833925225225
            ],
            [
              144.98421379938108,
              -37.84833925225225
            ],
            [
              144.98421379938108,
              -37.84383474774774
            ],
            [
              144.9796602006189,
              -37.84383474774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": ">500",
        "bed_cat": 5,
        "id": 919,
        "hospital_name": "The Alfred"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5893"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.9962331701589,
              -37.85706125225226
            ],
            [
              145.00079282984112,
              -37.85706125225226
            ],
            [
              145.00079282984112,
              -37.85255674774775
            ],
            [
              144.9962331701589,
              -37.85255674774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 920,
        "hospital_name": "The Avenue Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5894"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.03833710038373,
              -38.22577625225225
            ],
            [
              145.0435408996163,
              -38.22577625225225
            ],
            [
              145.0435408996163,
              -38.22127174774774
            ],
            [
              145.03833710038373,
              -38.22127174774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 921,
        "hospital_name": "The Bays Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5895"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.9554724807782,
              -37.304046252252256
            ],
            [
              144.96035751922182,
              -37.304046252252256
            ],
            [
              144.96035751922182,
              -37.299541747747746
            ],
            [
              144.9554724807782,
              -37.299541747747746
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 931,
        "hospital_name": "The Kilmore & District Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5896"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.3332617191898,
              -38.121047252252254
            ],
            [
              144.33819428081017,
              -38.121047252252254
            ],
            [
              144.33819428081017,
              -38.116542747747744
            ],
            [
              144.3332617191898,
              -38.116542747747744
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 932,
        "hospital_name": "The McKellar Centre"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5897"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.9964717068961,
              -37.81661325225225
            ],
            [
              145.0010062931039,
              -37.81661325225225
            ],
            [
              145.0010062931039,
              -37.81210874774774
            ],
            [
              144.9964717068961,
              -37.81210874774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 933,
        "hospital_name": "The Melbourne Clinic"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5898"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.03908499538028,
              -38.23242625225225
            ],
            [
              145.04430900461972,
              -38.23242625225225
            ],
            [
              145.04430900461972,
              -38.22792174774774
            ],
            [
              145.03908499538028,
              -38.22792174774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 935,
        "hospital_name": "The Mornington Centre"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_5899"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.01212639432788,
              -37.65566925225225
            ],
            [
              145.0166356056721,
              -37.65566925225225
            ],
            [
              145.0166356056721,
              -37.65116474774774
            ],
            [
              145.01212639432788,
              -37.65116474774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "200-500",
        "bed_cat": 4,
        "id": 936,
        "hospital_name": "The Northern Hospital [Epping]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_589a"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.1622629153332,
              -37.85496425225225
            ],
            [
              145.16682108466682,
              -37.85496425225225
            ],
            [
              145.16682108466682,
              -37.85045974774774
            ],
            [
              145.1622629153332,
              -37.85045974774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 938,
        "hospital_name": "The Peter James Centre [East Burwood]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_589b"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.9739790860589,
              -37.81123025225226
            ],
            [
              144.9785109139411,
              -37.81123025225226
            ],
            [
              144.9785109139411,
              -37.80672574774775
            ],
            [
              144.9739790860589,
              -37.80672574774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 941,
        "hospital_name": "The Royal Victorian Eye & Ear Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_589c"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.21080368695996,
              -37.940661252252255
            ],
            [
              145.21544031304003,
              -37.940661252252255
            ],
            [
              145.21544031304003,
              -37.936156747747745
            ],
            [
              145.21080368695996,
              -37.936156747747745
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 949,
        "hospital_name": "The Valley Private Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_589d"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.99494764906976,
              -37.84996625225226
            ],
            [
              144.99950235093021,
              -37.84996625225226
            ],
            [
              144.99950235093021,
              -37.84546174774775
            ],
            [
              144.99494764906976,
              -37.84546174774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 950,
        "hospital_name": "The Victoria Clinic"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_589e"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.15912735076037,
              -37.900479252252254
            ],
            [
              145.16372264923965,
              -37.900479252252254
            ],
            [
              145.16372264923965,
              -37.89597474774774
            ],
            [
              145.15912735076037,
              -37.89597474774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 951,
        "hospital_name": "The Victorian Rehabilitation Centre"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_589f"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.88515782321124,
              -37.799659252252255
            ],
            [
              144.88968417678873,
              -37.799659252252255
            ],
            [
              144.88968417678873,
              -37.795154747747745
            ],
            [
              144.88515782321124,
              -37.795154747747745
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 978,
        "hospital_name": "Tweddle Child & Family Health Centre [Footscray]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_58a0"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.97249420004542,
              -37.810773252252254
            ],
            [
              144.9770257999546,
              -37.810773252252254
            ],
            [
              144.9770257999546,
              -37.806268747747744
            ],
            [
              144.97249420004542,
              -37.806268747747744
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 985,
        "hospital_name": "Victoria Parade Surgery Centre"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_58a1"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.01071355475437,
              -37.791562252252255
            ],
            [
              145.01523644524565,
              -37.791562252252255
            ],
            [
              145.01523644524565,
              -37.787057747747745
            ],
            [
              145.01071355475437,
              -37.787057747747745
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 986,
        "hospital_name": "Victorian Institute of Forensic Mental Health"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_58a2"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.94849012334635,
              -37.81501525225225
            ],
            [
              144.95302387665367,
              -37.81501525225225
            ],
            [
              144.95302387665367,
              -37.81051074774774
            ],
            [
              144.94849012334635,
              -37.81051074774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 987,
        "hospital_name": "Victorian Institute of Forensic Mental Health Prison Health Service"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_58a3"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.05332755553871,
              -37.83477225225226
            ],
            [
              145.05787244446128,
              -37.83477225225226
            ],
            [
              145.05787244446128,
              -37.83026774774775
            ],
            [
              145.05332755553871,
              -37.83026774774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": null,
        "bed_cat": null,
        "id": 988,
        "hospital_name": "Vision Day Surgery Camberwell"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_58a4"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.11716484697686,
              -37.819843252252255
            ],
            [
              145.1217011530231,
              -37.819843252252255
            ],
            [
              145.1217011530231,
              -37.815338747747745
            ],
            [
              145.11716484697686,
              -37.815338747747745
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": null,
        "bed_cat": null,
        "id": 990,
        "hospital_name": "Vision Day Surgery Eastern"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_58a5"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.89289112003564,
              -37.802760252252256
            ],
            [
              144.89741887996433,
              -37.802760252252256
            ],
            [
              144.89741887996433,
              -37.798255747747746
            ],
            [
              144.89289112003564,
              -37.798255747747746
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": null,
        "bed_cat": null,
        "id": 991,
        "hospital_name": "Vision Day Surgery Footscray"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_58a6"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.97802565348326,
              -37.85285725225226
            ],
            [
              144.98258234651672,
              -37.85285725225226
            ],
            [
              144.98258234651672,
              -37.84835274774775
            ],
            [
              144.97802565348326,
              -37.84835274774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": null,
        "bed_cat": null,
        "id": 994,
        "hospital_name": "Vision Eye Institute"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_58a7"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.22362437179083,
              -37.85077725225226
            ],
            [
              145.22817962820915,
              -37.85077725225226
            ],
            [
              145.22817962820915,
              -37.84627274774775
            ],
            [
              145.22362437179083,
              -37.84627274774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 1006,
        "hospital_name": "Wantirna Health"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_58a8"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.05847229866544,
              -37.756671252252254
            ],
            [
              145.0629837013346,
              -37.756671252252254
            ],
            [
              145.0629837013346,
              -37.752166747747744
            ],
            [
              145.05847229866544,
              -37.752166747747744
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 1013,
        "hospital_name": "Warringal Private Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_58a9"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.14350642532688,
              -37.88688425225226
            ],
            [
              145.14808957467315,
              -37.88688425225226
            ],
            [
              145.14808957467315,
              -37.88237974774775
            ],
            [
              145.14350642532688,
              -37.88237974774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 1016,
        "hospital_name": "Waverley Private Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_58aa"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.69632857983234,
              -37.888839252252254
            ],
            [
              144.70091342016767,
              -37.888839252252254
            ],
            [
              144.70091342016767,
              -37.884334747747744
            ],
            [
              144.69632857983234,
              -37.884334747747744
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "100-199",
        "bed_cat": 3,
        "id": 1022,
        "hospital_name": "Werribee Mercy Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_58ab"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.8849289333442,
              -37.79455025225226
            ],
            [
              144.8894530666558,
              -37.79455025225226
            ],
            [
              144.8894530666558,
              -37.79004574774775
            ],
            [
              144.8849289333442,
              -37.79004574774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "200-500",
        "bed_cat": 4,
        "id": 1035,
        "hospital_name": "Western Hospital [Footscray]"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_58ac"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.88366977478202,
              -37.795297252252254
            ],
            [
              144.88819422521797,
              -37.795297252252254
            ],
            [
              144.88819422521797,
              -37.790792747747744
            ],
            [
              144.88366977478202,
              -37.790792747747744
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 1036,
        "hospital_name": "Western Private Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_58ad"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              144.89005594066379,
              -37.86582825225226
            ],
            [
              144.8946220593362,
              -37.86582825225226
            ],
            [
              144.8946220593362,
              -37.86132374774775
            ],
            [
              144.89005594066379,
              -37.86132374774775
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "50-99",
        "bed_cat": 2,
        "id": 1044,
        "hospital_name": "Williamstown Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_58ae"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.348948057289,
              -37.75857125225225
            ],
            [
              145.353459942711,
              -37.75857125225225
            ],
            [
              145.353459942711,
              -37.75406674774774
            ],
            [
              145.348948057289,
              -37.75406674774774
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 1068,
        "hospital_name": "Yarra Ranges Health"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_58af"
    },
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              145.4281775056005,
              -37.215886252252254
            ],
            [
              145.4332704943995,
              -37.215886252252254
            ],
            [
              145.4332704943995,
              -37.211381747747744
            ],
            [
              145.4281775056005,
              -37.211381747747744
            ]
          ]
        ]
      },
      "type": "Feature",
      "properties": {
        "beds": "<50",
        "bed_cat": 1,
        "id": 1072,
        "hospital_name": "Yea & District Memorial Hospital"
      },
      "id": "my_hospital_beds.fid-75f06f09_15b6036e53e_58b0"
    }
  ]
}