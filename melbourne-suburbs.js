var suburbs = {
	"type": "FeatureCollection",
	"features": [
	{
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [144.9842, -37.8507],
            [144.9133, -37.8421],
            [144.8984, -37.8412],
            [144.9056, -37.8229],
            [144.9027, -37.7896],
            [144.9371, -37.7778],
            [144.9615, -37.7782],
            [144.9879, -37.8296],
            [144.9842, -37.8507]
          ]
        ]
      },
      "properties": {
        "Name": "Melbourne (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  76034],           ['2006',  80154],           ['2007',  85366],           ['2008',  89937],           ['2009',  94247],           ['2010',  97623],           ['2011',  100240],           ['2012',  105418],           ['2013',  116330],           ['2014',  122167],           ['2015',  128980]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>24600</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Melbourne (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>76034</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>122167</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>128980</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>6813</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>5.6</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>37.4</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>3452.8</td> </tr></table></center>",
        "rmapshaperid": 49
      },
      "id": 50
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "MultiPolygon",
        "coordinates": [
          [
            [
              [139.4277, -17.1259],
              [139.401, -17.106],
              [139.4954, -16.9919],
              [139.5435, -17.0314],
              [139.5662, -17.0926],
              [139.4277, -17.1259]
            ]
          ],
          [
            [
              [139.195, -16.6619],
              [139.1606, -16.6102],
              [139.2072, -16.5745],
              [139.2418, -16.5043],
              [139.3154, -16.4596],
              [139.4654, -16.4461],
              [139.547, -16.3911],
              [139.6053, -16.4035],
              [139.6764, -16.4506],
              [139.6994, -16.5087],
              [139.596, -16.5542],
              [139.5783, -16.5027],
              [139.498, -16.5148],
              [139.4712, -16.5575],
              [139.4513, -16.6663],
              [139.3686, -16.6721],
              [139.2881, -16.7344],
              [139.23, -16.7312],
              [139.195, -16.6619]
            ]
          ]
        ]
      },
      "properties": {
        "Name": "Mornington (S)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  1067],           ['2006',  1082],           ['2007',  1119],           ['2008',  1120],           ['2009',  1158],           ['2010',  1184],           ['2011',  1220],           ['2012',  1219],           ['2013',  1213],           ['2014',  1223],           ['2015',  1225]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>35250</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Mornington (S)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>1067</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>1223</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>1225</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>2</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>0.2</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>1244.2</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>1.0</td> </tr></table></center>",
        "rmapshaperid": 474
      },
      "id": 480
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [145.2158, -38.2007],
            [145.1288, -38.2001],
            [145.0979, -38.1624],
            [145.1228, -38.085],
            [145.1563, -38.0674],
            [145.2396, -38.0777],
            [145.2158, -38.2007]
          ]
        ]
      },
      "properties": {
        "Name": "Frankston (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  118530],           ['2006',  120148],           ['2007',  122182],           ['2008',  124689],           ['2009',  127252],           ['2010',  129052],           ['2011',  130350],           ['2012',  131754],           ['2013',  133425],           ['2014',  134899],           ['2015',  135971]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>22170</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Frankston (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>118530</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>134899</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>135971</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>1072</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>0.8</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>129.6</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>1049.2</td> </tr></table></center>",
        "rmapshaperid": 401
      },
      "id": 406
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [145.2296, -37.9523],
            [145.2932, -37.9649],
            [145.3792, -37.975],
            [145.3659, -38.0388],
            [145.4018, -38.1191],
            [145.3889, -38.1925],
            [145.43, -38.2152],
            [145.3534, -38.2147],
            [145.3114, -38.2463],
            [145.2542, -38.2236],
            [145.2158, -38.2007],
            [145.2396, -38.0777],
            [145.2519, -38.0036],
            [145.2296, -37.9523]
          ]
        ]
      },
      "properties": {
        "Name": "Casey (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  213821],           ['2006',  220440],           ['2007',  228496],           ['2008',  237529],           ['2009',  246678],           ['2010',  254471],           ['2011',  261282],           ['2012',  267640],           ['2013',  274836],           ['2014',  283215],           ['2015',  292211]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>21610</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Casey (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>213821</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>283215</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>292211</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>8996</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>3.2</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>409.4</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>713.7</td> </tr></table></center>",
        "rmapshaperid": 324
      },
      "id": 329
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [145.1228, -38.085],
            [145.0517, -37.9868],
            [145.0344, -37.933],
            [145.0825, -37.924],
            [145.1413, -37.9313],
            [145.1378, -38.0324],
            [145.1563, -38.0674],
            [145.1228, -38.085]
          ]
        ]
      },
      "properties": {
        "Name": "Kingston (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  137091],           ['2006',  138390],           ['2007',  140487],           ['2008',  142817],           ['2009',  146050],           ['2010',  147407],           ['2011',  148304],           ['2012',  150014],           ['2013',  151533],           ['2014',  153034],           ['2015',  154477]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>23430</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Kingston (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>137091</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>153034</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>154477</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>1443</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>0.9</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>91.4</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>1690.7</td> </tr></table></center>",
        "rmapshaperid": 394
      },
      "id": 399
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [145.2192, -37.9401],
            [145.2296, -37.9523],
            [145.2519, -38.0036],
            [145.2396, -38.0777],
            [145.1563, -38.0674],
            [145.1378, -38.0324],
            [145.1413, -37.9313],
            [145.2192, -37.9401]
          ]
        ]
      },
      "properties": {
        "Name": "Greater Dandenong (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  128054],           ['2006',  130068],           ['2007',  132860],           ['2008',  135884],           ['2009',  138740],           ['2010',  140212],           ['2011',  142167],           ['2012',  144312],           ['2013',  146578],           ['2014',  149466],           ['2015',  152739]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>22670</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Greater Dandenong (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>128054</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>149466</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>152739</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>3273</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>2.2</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>129.6</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>1178.6</td> </tr></table></center>",
        "rmapshaperid": 28
      },
      "id": 29
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [144.985, -37.891],
            [144.997, -37.8838],
            [145.0344, -37.933],
            [145.0517, -37.9868],
            [144.9855, -37.9262],
            [144.985, -37.891]
          ]
        ]
      },
      "properties": {
        "Name": "Bayside (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  89873],           ['2006',  90807],           ['2007',  92041],           ['2008',  93535],           ['2009',  94850],           ['2010',  95594],           ['2011',  96119],           ['2012',  96992],           ['2013',  98270],           ['2014',  99914],           ['2015',  101321]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>20910</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Bayside (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>89873</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>99914</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>101321</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>1407</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>1.4</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>37.2</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>2722.9</td> </tr></table></center>",
        "rmapshaperid": 553
      },
      "id": 563
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [145.2192, -37.9401],
            [145.1413, -37.9313],
            [145.0825, -37.924],
            [145.0782, -37.8872],
            [145.0922, -37.8719],
            [145.0951, -37.8533],
            [145.1957, -37.8652],
            [145.2192, -37.9401]
          ]
        ]
      },
      "properties": {
        "Name": "Monash (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  164949],           ['2006',  167010],           ['2007',  169145],           ['2008',  171321],           ['2009',  174398],           ['2010',  175731],           ['2011',  177345],           ['2012',  179931],           ['2013',  182300],           ['2014',  184977],           ['2015',  187286]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>24970</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Monash (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>164949</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>184977</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>187286</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>2309</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>1.2</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>81.5</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>2298.1</td> </tr></table></center>",
        "rmapshaperid": 29
      },
      "id": 30
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [144.997, -37.8838],
            [145.0105, -37.8602],
            [145.0782, -37.8872],
            [145.0825, -37.924],
            [145.0344, -37.933],
            [144.997, -37.8838]
          ]
        ]
      },
      "properties": {
        "Name": "Glen Eira (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  126485],           ['2006',  128474],           ['2007',  130524],           ['2008',  132900],           ['2009',  135414],           ['2010',  136515],           ['2011',  137152],           ['2012',  139242],           ['2013',  141379],           ['2014',  144009],           ['2015',  146303]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>22310</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Glen Eira (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>126485</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>144009</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>146303</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>2294</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>1.6</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>38.7</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>3781.3</td> </tr></table></center>",
        "rmapshaperid": 165
      },
      "id": 169
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [145.2296, -37.9523],
            [145.2192, -37.9401],
            [145.1957, -37.8652],
            [145.2133, -37.841],
            [145.318, -37.8439],
            [145.2932, -37.9649],
            [145.2296, -37.9523]
          ]
        ]
      },
      "properties": {
        "Name": "Knox (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  149362],           ['2006',  150275],           ['2007',  151343],           ['2008',  152538],           ['2009',  154122],           ['2010',  154643],           ['2011',  154625],           ['2012',  154583],           ['2013',  154760],           ['2014',  155279],           ['2015',  155681]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>23670</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Knox (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>149362</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>155279</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>155681</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>402</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>0.3</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>113.8</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>1367.7</td> </tr></table></center>",
        "rmapshaperid": 123
      },
      "id": 126
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [145.318, -37.8439],
            [145.2133, -37.841],
            [145.2135, -37.8118],
            [145.2896, -37.7628],
            [145.318, -37.8439]
          ]
        ]
      },
      "properties": {
        "Name": "Maroondah (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  101009],           ['2006',  101430],           ['2007',  102536],           ['2008',  104034],           ['2009',  106011],           ['2010',  106677],           ['2011',  107323],           ['2012',  108168],           ['2013',  109466],           ['2014',  111185],           ['2015',  112310]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>24410</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Maroondah (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>101009</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>111185</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>112310</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>1125</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>1.0</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>61.4</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>1828.8</td> </tr></table></center>",
        "rmapshaperid": 59
      },
      "id": 60
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [145.2135, -37.8118],
            [145.1067, -37.7922],
            [145.0671, -37.7809],
            [145.1357, -37.7409],
            [145.2936, -37.7122],
            [145.2896, -37.7628],
            [145.2135, -37.8118]
          ]
        ]
      },
      "properties": {
        "Name": "Manningham (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  113432],           ['2006',  113914],           ['2007',  114771],           ['2008',  115539],           ['2009',  116372],           ['2010',  116656],           ['2011',  116750],           ['2012',  117036],           ['2013',  117409],           ['2014',  118485],           ['2015',  119442]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>24210</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Manningham (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>113432</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>118485</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>119442</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>957</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>0.8</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>113.3</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>1053.8</td> </tr></table></center>",
        "rmapshaperid": 435
      },
      "id": 441
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [145.0951, -37.8533],
            [145.1067, -37.7922],
            [145.2135, -37.8118],
            [145.2133, -37.841],
            [145.1957, -37.8652],
            [145.0951, -37.8533]
          ]
        ]
      },
      "properties": {
        "Name": "Whitehorse (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  147344],           ['2006',  149017],           ['2007',  151180],           ['2008',  153487],           ['2009',  155959],           ['2010',  157058],           ['2011',  157538],           ['2012',  159296],           ['2013',  161560],           ['2014',  163646],           ['2015',  165557]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>26980</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Whitehorse (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>147344</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>163646</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>165557</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>1911</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>1.2</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>64.3</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>2575.6</td> </tr></table></center>",
        "rmapshaperid": 194
      },
      "id": 199
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [144.9842, -37.8507],
            [145.0105, -37.8602],
            [144.997, -37.8838],
            [144.985, -37.891],
            [144.9133, -37.8421],
            [144.9842, -37.8507]
          ]
        ]
      },
      "properties": {
        "Name": "Port Phillip (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  87831],           ['2006',  89641],           ['2007',  91490],           ['2008',  93174],           ['2009',  95272],           ['2010',  96375],           ['2011',  97276],           ['2012',  100394],           ['2013',  102396],           ['2014',  104813],           ['2015',  107127]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>25900</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Port Phillip (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>87831</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>104813</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>107127</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>2314</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>2.2</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>20.7</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>5174.0</td> </tr></table></center>",
        "rmapshaperid": 58
      },
      "id": 59
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [145.0244, -37.8345],
            [145.0402, -37.7842],
            [145.0671, -37.7809],
            [145.1067, -37.7922],
            [145.0951, -37.8533],
            [145.0922, -37.8719],
            [145.0244, -37.8345]
          ]
        ]
      },
      "properties": {
        "Name": "Boroondara (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  158727],           ['2006',  159605],           ['2007',  161308],           ['2008',  163408],           ['2009',  165284],           ['2010',  166326],           ['2011',  167062],           ['2012',  168458],           ['2013',  170382],           ['2014',  172551],           ['2015',  174787]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>21110</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Boroondara (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>158727</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>172551</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>174787</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>2236</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>1.3</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>60.2</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>2904.4</td> </tr></table></center>",
        "rmapshaperid": 186
      },
      "id": 191
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [144.9842, -37.8507],
            [144.9879, -37.8296],
            [145.0244, -37.8345],
            [145.0922, -37.8719],
            [145.0782, -37.8872],
            [145.0105, -37.8602],
            [144.9842, -37.8507]
          ]
        ]
      },
      "properties": {
        "Name": "Stonnington (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  92893],           ['2006',  94056],           ['2007',  95228],           ['2008',  96841],           ['2009',  98186],           ['2010',  98728],           ['2011',  98853],           ['2012',  101187],           ['2013',  103085],           ['2014',  105946],           ['2015',  107941]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>26350</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Stonnington (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>92893</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>105946</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>107941</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>1995</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>1.9</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>25.6</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>4208.4</td> </tr></table></center>",
        "rmapshaperid": 450
      },
      "id": 456
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [145.2158, -38.2007],
            [145.2542, -38.2236],
            [145.1963, -38.298],
            [145.2086, -38.4062],
            [145.1298, -38.3905],
            [145.0502, -38.43],
            [145.011, -38.4797],
            [144.9244, -38.4974],
            [144.7524, -38.3639],
            [144.8509, -38.3687],
            [144.9884, -38.3167],
            [145.0354, -38.2149],
            [145.0979, -38.1624],
            [145.1288, -38.2001],
            [145.2158, -38.2007]
          ]
        ]
      },
      "properties": {
        "Name": "Mornington Peninsula (S)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  137759],           ['2006',  139317],           ['2007',  141740],           ['2008',  144403],           ['2009',  147002],           ['2010',  148542],           ['2011',  149271],           ['2012',  150830],           ['2013',  152106],           ['2014',  153749],           ['2015',  155015]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>25340</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Mornington Peninsula (S)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>137759</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>153749</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>155015</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>1266</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>0.8</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>724.1</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>214.1</td> </tr></table></center>",
        "rmapshaperid": 524
      },
      "id": 533
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [144.9885, -37.7764],
            [145.036, -37.7789],
            [145.0402, -37.7842],
            [145.0244, -37.8345],
            [144.9879, -37.8296],
            [144.9615, -37.7782],
            [144.9885, -37.7764]
          ]
        ]
      },
      "properties": {
        "Name": "Yarra (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  71719],           ['2006',  72808],           ['2007',  74289],           ['2008',  75868],           ['2009',  77311],           ['2010',  78430],           ['2011',  78903],           ['2012',  80987],           ['2013',  83508],           ['2014',  86377],           ['2015',  89151]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>27350</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Yarra (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>71719</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>86377</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>89151</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>2774</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>3.2</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>19.5</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>4562.2</td> </tr></table></center>",
        "rmapshaperid": 46
      },
      "id": 47
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [145.0402, -37.7842],
            [145.036, -37.7789],
            [145.0582, -37.7],
            [145.0677, -37.6869],
            [145.1437, -37.6826],
            [145.1357, -37.7409],
            [145.0671, -37.7809],
            [145.0402, -37.7842]
          ]
        ]
      },
      "properties": {
        "Name": "Banyule (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  117149],           ['2006',  117963],           ['2007',  119362],           ['2008',  120580],           ['2009',  122248],           ['2010',  122780],           ['2011',  122983],           ['2012',  123600],           ['2013',  124350],           ['2014',  125451],           ['2015',  126232]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>20660</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Banyule (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>117149</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>125451</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>126232</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>781</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>0.6</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>62.5</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>2018.4</td> </tr></table></center>",
        "rmapshaperid": 131
      },
      "id": 135
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [145.2393, -37.4877],
            [145.3664, -37.5451],
            [145.3539, -37.6439],
            [145.2936, -37.7122],
            [145.1357, -37.7409],
            [145.1437, -37.6826],
            [145.0677, -37.6869],
            [145.1358, -37.6199],
            [145.1444, -37.5703],
            [145.2284, -37.5214],
            [145.2393, -37.4877]
          ]
        ]
      },
      "properties": {
        "Name": "Nillumbik (S)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  61102],           ['2006',  61515],           ['2007',  61858],           ['2008',  62367],           ['2009',  62785],           ['2010',  62838],           ['2011',  62716],           ['2012',  62751],           ['2013',  62660],           ['2014',  62849],           ['2015',  62602]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>25710</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Nillumbik (S)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>61102</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>62849</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>62602</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>-247</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>-0.4</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>432.3</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>144.8</td> </tr></table></center>",
        "rmapshaperid": 39
      },
      "id": 40
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [144.9885, -37.7764],
            [144.9721, -37.691],
            [145.0582, -37.7],
            [145.036, -37.7789],
            [144.9885, -37.7764]
          ]
        ]
      },
      "properties": {
        "Name": "Darebin (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  130298],           ['2006',  132299],           ['2007',  134748],           ['2008',  137525],           ['2009',  140289],           ['2010',  141780],           ['2011',  142942],           ['2012',  144641],           ['2013',  146650],           ['2014',  148675],           ['2015',  150881]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>21890</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Darebin (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>130298</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>148675</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>150881</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>2206</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>1.5</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>53.5</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>2821.7</td> </tr></table></center>",
        "rmapshaperid": 176
      },
      "id": 180
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [144.9615, -37.7782],
            [144.9371, -37.7778],
            [144.8885, -37.7108],
            [144.8862, -37.7077],
            [144.9721, -37.691],
            [144.9885, -37.7764],
            [144.9615, -37.7782]
          ]
        ]
      },
      "properties": {
        "Name": "Moreland (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  138963],           ['2006',  140872],           ['2007',  143694],           ['2008',  146450],           ['2009',  150114],           ['2010',  152261],           ['2011',  154247],           ['2012',  156657],           ['2013',  159867],           ['2014',  163331],           ['2015',  166770]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>25250</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Moreland (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>138963</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>163331</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>166770</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>3439</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>2.1</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>50.9</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>3273.3</td> </tr></table></center>",
        "rmapshaperid": 30
      },
      "id": 31
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [144.8514, -37.7702],
            [144.8885, -37.7108],
            [144.9371, -37.7778],
            [144.9027, -37.7896],
            [144.8514, -37.7702]
          ]
        ]
      },
      "properties": {
        "Name": "Moonee Valley (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  105059],           ['2006',  105717],           ['2007',  106716],           ['2008',  108417],           ['2009',  110367],           ['2010',  111363],           ['2011',  112180],           ['2012',  113513],           ['2013',  114979],           ['2014',  117297],           ['2015',  119583]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>25060</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Moonee Valley (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>105059</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>117297</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>119583</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>2286</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>1.9</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>43.1</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>2771.8</td> </tr></table></center>",
        "rmapshaperid": 181
      },
      "id": 185
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [144.8514, -37.7702],
            [144.9027, -37.7896],
            [144.9056, -37.8229],
            [144.8559, -37.8149],
            [144.8514, -37.7702]
          ]
        ]
      },
      "properties": {
        "Name": "Maribyrnong (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  64137],           ['2006',  65518],           ['2007',  67666],           ['2008',  69902],           ['2009',  71872],           ['2010',  73613],           ['2011',  75154],           ['2012',  76866],           ['2013',  79222],           ['2014',  81831],           ['2015',  83515]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>24330</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Maribyrnong (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>64137</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>81831</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>83515</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>1684</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>2.1</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>31.2</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>2674.5</td> </tr></table></center>",
        "rmapshaperid": 37
      },
      "id": 38
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [144.8559, -37.8149],
            [144.9056, -37.8229],
            [144.8984, -37.8412],
            [144.8045, -37.8944],
            [144.8274, -37.8228],
            [144.8559, -37.8149]
          ]
        ]
      },
      "properties": {
        "Name": "Hobsons Bay (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  82990],           ['2006',  83906],           ['2007',  84819],           ['2008',  85513],           ['2009',  86767],           ['2010',  87192],           ['2011',  87395],           ['2012',  88275],           ['2013',  89384],           ['2014',  91118],           ['2015',  92761]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>23110</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Hobsons Bay (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>82990</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>91118</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>92761</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>1643</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>1.8</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>64.2</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>1444.0</td> </tr></table></center>",
        "rmapshaperid": 120
      },
      "id": 123
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [144.5916, -38.0044],
            [144.4799, -37.9499],
            [144.4441, -37.8641],
            [144.4777, -37.781],
            [144.751, -37.8105],
            [144.8274, -37.8228],
            [144.8045, -37.8944],
            [144.6516, -38.0016],
            [144.5916, -38.0044]
          ]
        ]
      },
      "properties": {
        "Name": "Wyndham (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  108795],           ['2006',  115161],           ['2007',  123778],           ['2008',  133063],           ['2009',  143405],           ['2010',  155251],           ['2011',  166699],           ['2012',  178859],           ['2013',  189063],           ['2014',  199645],           ['2015',  209847]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>27260</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Wyndham (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>108795</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>199645</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>209847</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>10202</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>5.1</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>542.1</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>387.1</td> </tr></table></center>",
        "rmapshaperid": 158
      },
      "id": 162
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [144.751, -37.8105],
            [144.4777, -37.781],
            [144.5194, -37.7062],
            [144.5227, -37.6046],
            [144.5562, -37.5589],
            [144.6716, -37.5677],
            [144.7465, -37.663],
            [144.7682, -37.7097],
            [144.751, -37.8105]
          ]
        ]
      },
      "properties": {
        "Name": "Melton (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  74316],           ['2006',  80595],           ['2007',  86196],           ['2008',  92552],           ['2009',  99935],           ['2010',  106848],           ['2011',  112643],           ['2012',  117881],           ['2013',  122786],           ['2014',  127638],           ['2015',  132752]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>24650</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Melton (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>74316</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>127638</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>132752</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>5114</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>4.0</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>527.6</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>251.6</td> </tr></table></center>",
        "rmapshaperid": 109
      },
      "id": 112
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [144.8862, -37.7077],
            [144.8885, -37.7108],
            [144.8514, -37.7702],
            [144.8559, -37.8149],
            [144.8274, -37.8228],
            [144.751, -37.8105],
            [144.7682, -37.7097],
            [144.7465, -37.663],
            [144.8288, -37.7069],
            [144.8862, -37.7077]
          ]
        ]
      },
      "properties": {
        "Name": "Brimbank (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  172749],           ['2006',  174231],           ['2007',  177004],           ['2008',  180561],           ['2009',  185397],           ['2010',  188895],           ['2011',  191496],           ['2012',  193334],           ['2013',  195273],           ['2014',  197637],           ['2015',  199432]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>21180</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Brimbank (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>172749</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>197637</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>199432</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>1795</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>0.9</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>123.4</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>1616.2</td> </tr></table></center>",
        "rmapshaperid": 90
      },
      "id": 92
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [144.7465, -37.663],
            [144.6716, -37.5677],
            [144.6381, -37.4995],
            [144.7262, -37.4865],
            [144.8835, -37.5034],
            [144.9772, -37.5137],
            [144.9538, -37.5498],
            [144.9721, -37.691],
            [144.8862, -37.7077],
            [144.8288, -37.7069],
            [144.7465, -37.663]
          ]
        ]
      },
      "properties": {
        "Name": "Hume (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  149059],           ['2006',  152797],           ['2007',  157516],           ['2008',  161863],           ['2009',  166728],           ['2010',  170952],           ['2011',  174290],           ['2012',  178034],           ['2013',  183077],           ['2014',  188669],           ['2015',  194006]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>23270</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Hume (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>149059</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>188669</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>194006</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>5337</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>2.8</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>503.8</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>385.1</td> </tr></table></center>",
        "rmapshaperid": 72
      },
      "id": 73
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [145.0582, -37.7],
            [144.9721, -37.691],
            [144.9538, -37.5498],
            [144.9772, -37.5137],
            [145.0008, -37.4424],
            [145.0729, -37.4506],
            [145.1046, -37.4124],
            [145.1659, -37.4131],
            [145.2223, -37.4726],
            [145.2393, -37.4877],
            [145.2284, -37.5214],
            [145.1444, -37.5703],
            [145.1358, -37.6199],
            [145.0677, -37.6869],
            [145.0582, -37.7]
          ]
        ]
      },
      "properties": {
        "Name": "Whittlesea (C)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  125893],           ['2006',  128491],           ['2007',  132938],           ['2008',  138446],           ['2009',  145445],           ['2010',  152709],           ['2011',  160800],           ['2012',  169955],           ['2013',  179080],           ['2014',  186843],           ['2015',  195397]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>27070</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Whittlesea (C)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>125893</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>186843</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>195397</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>8554</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>4.6</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>489.7</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>399.0</td> </tr></table></center>",
        "rmapshaperid": 183
      },
      "id": 187
    },
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [145.3664, -37.5451],
            [145.618, -37.5723],
            [145.6759, -37.6242],
            [145.8369, -37.5319],
            [145.8833, -37.527],
            [146.186, -37.5348],
            [146.1921, -37.5877],
            [146.1306, -37.6442],
            [146.1573, -37.7649],
            [146.0387, -37.7743],
            [145.9447, -37.7591],
            [145.896, -37.8254],
            [145.8449, -37.7977],
            [145.8115, -37.8612],
            [145.7479, -37.9074],
            [145.7215, -37.8852],
            [145.6279, -37.904],
            [145.5371, -37.867],
            [145.3841, -37.9296],
            [145.3792, -37.975],
            [145.2932, -37.9649],
            [145.318, -37.8439],
            [145.2896, -37.7628],
            [145.2936, -37.7122],
            [145.3539, -37.6439],
            [145.3664, -37.5451]
          ]
        ]
      },
      "properties": {
        "Name": "Yarra Ranges (S)",
        "Description": "<html>   <head>     <script type=\"text/javascript\"           src=\"https://www.google.com/jsapi?autoload={             'modules':[{               'name':'visualization',               'version':'1',               'packages':['corechart']             }]           }\"></script>      <script type=\"text/javascript\">       google.setOnLoadCallback(drawChart);        function drawChart() {         var data = google.visualization.arrayToDataTable([           ['Year', 'Population'],           ['2005',  142928],           ['2006',  143393],           ['2007',  144485],           ['2008',  145884],           ['2009',  147663],           ['2010',  148473],           ['2011',  148901],           ['2012',  149235],           ['2013',  149390],           ['2014',  150036],           ['2015',  150661]            ]);          var options = {           title: 'Population'               ,'legend':'none'               ,chartArea:{width:'80%'}          };          var chart = new google.visualization.LineChart(document.getElementById('chart_div'));          chart.draw(data, options);       }     </script>   </head>   <body>     <div id=\"chart_div\" style=\"width: 600px; height: 300px\"></div>   </body> </html> <center><table><tr bgcolor=\"#E3E3F3\"> <th><p align=left>LGA code</p></th> <td>27450</td> </tr><tr bgcolor=\"\"> <th><p align=left>LGA name</p></th> <td>Yarra Ranges (S)</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2005</p></th> <td>142928</td> </tr><tr bgcolor=\"\"> <th><p align=left>Population 2014</p></th> <td>150036</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population 2015</p></th> <td>150661</td> </tr><tr bgcolor=\"\"> <th><p align=left>Change 2014 to 2015 (no.)</p></th> <td>625</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Change 2014 to 2015 (%)</p></th> <td>0.4</td> </tr><tr bgcolor=\"\"> <th><p align=left>Area (km<sup>2</sup>)</p></th> <td>2465.7</td> </tr><tr bgcolor=\"#E3E3F3\"> <th><p align=left>Population density 2015 (persons per km<sup>2</sup>)</p></th> <td>61.1</td> </tr></table></center>",
        "rmapshaperid": 400
      },
      "id": 405
    },
	]
}