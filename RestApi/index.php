<?php
/**
 * Team 35 Melbourne
 * Zhe Xu -----------741434
 * Qi Huang --------732802
 * Botian Chen ----750249
 * Han Yu -----------744981
 * Kun He-----------721391
 */

require 'vendor/autoload.php';
require_once 'lib/Couch.php';
require_once 'lib/CouchAdmin.php';
require_once 'lib/CouchClient.php';

use PHPOnCouch\Couch;
use PHPOnCouch\CouchAdmin;
use PHPOnCouch\CouchClient;

$app = new \Slim\App;

$app->get('/', function($request, $response, $args) {
   return $response->write("Hello world");
});

$app->get('/twitterCounts/positive', function($request, $response, $args) {
    $client = getDb();
    $doc = $client->getDoc('sr1');
    $items = $doc->fields->suburbs;
    $data_send = array();
    $index = 0;
    foreach($items as $item) {
        $data_send[$index]['id'] = $item->id;
        $data_send[$index]['count'] = $item->count;
        $index++;
    }

    return $response->write(json_encode(array('type'=>'twitter_count', 'data'=>$data_send)));
});

$app->get('/twitterCounts/negative', function($request, $response, $args) {
    $client = getDb();
    $doc = $client->getDoc('sr2');
    $items = $doc->fields->suburbs;
    $data_send = array();
    $index = 0;
    foreach($items as $item) {
        $data_send[$index]['id'] = $item->id;
        $data_send[$index]['count'] = $item->count;
        $index++;
    }

    return $response->write(json_encode(array('type'=>'twitter_count', 'data'=>$data_send)));
});

function getDb() {
    $couchdb_server = "http://130.56.251.205:5984";
    $couchdb_database_name = "suburbs_result";
    $client = new CouchClient($couchdb_server, $couchdb_database_name);

    return $client;
}

$app->run();